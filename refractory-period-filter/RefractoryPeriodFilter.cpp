#include <dv-sdk/module.hpp>
#include <dv-sdk/processing.hpp>

class RefractoryPeriodFilter : public dv::ModuleBase {
private:
	// user selectable refractory period in microseconds
	int64_t refractoryPeriod;

	// a matrix storing the last firing times for every pixel
	dv::TimeSurfaceBase<dv::EventStore> lastFiringTimes;

public:
	static void initInputs(dv::InputDefinitionList &in) {
		in.addEventInput("events");
	}

	static void initOutputs(dv::OutputDefinitionList &out) {
		out.addEventOutput("events");
	}

	static const char *initDescription() {
		return ("This module filters events by applying a refractory period to the event timestamps.");
	}

	static void initConfigOptions(dv::RuntimeConfig &config) {
		config.add("refractoryPeriod",
			dv::ConfigOption::longOption("Refractory period to apply to events (in ms)", 10, 1, 1000));

		config.setPriorityOptions({"refractoryPeriod"});
	}

	RefractoryPeriodFilter() : refractoryPeriod(0), lastFiringTimes(inputs.getEventInput("events").size()) {
		outputs.getEventOutput("events").setup(inputs.getEventInput("events"));
	}

	void run() override {
		auto input  = inputs.getEventInput("events");
		auto output = outputs.getEventOutput("events");

		// Filter events, add them to output if valid.
		for (const auto &event : input.events()) {
			if (event.timestamp() - lastFiringTimes.at(event.y(), event.x()) >= refractoryPeriod) {
				output << event;
			}

			lastFiringTimes.at(event.y(), event.x()) = event.timestamp();
		}

		output << dv::commit;
	}

	void configUpdate() override {
		refractoryPeriod = config.getLong("refractoryPeriod") * 1000;
	}
};

registerModuleClass(RefractoryPeriodFilter)
