## DV Tutorial code

This repository contains all the code to the tutorials and coding instructions from the DV documentation. 

The documentation as well as the tutorials can be found on **[https://inivation.gitlab.io/dv/dv-docs](https://inivation.gitlab.io/dv/dv-docs/)**


### Contents

#### [Refractory period filter](refractory-period-filter)
A simple event filter. Showcases minimal, useful module configuration

#### [Color paint example](color-paint-example)
A slighlty more complex filter, showcasing frame outputs, OpenCV integration as well as header / source file separation


